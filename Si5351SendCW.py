#!/usr/bin/python
#
# An attempt at making the Si5351 send Morse code at 
# a desired frequency. This does require you to have
# installed the Si5351 library from: 
# https://github.com/anitracks/RasPi-Si5351.py.git
# at a similar directory depth.
#
# Morse code dictionary came from:
# https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/robot/morse_code/
#
# Seth McNeill
# 2015 November 18

import time
import datetime
import sys
# add the Si5351 library to the path
sys.path.insert(0, '../RasPi-Si5351.py')
import Si5351
import morse # morse.py which contains the CODE dictionary

def sendTxt(txt, dotlen):
  dashlen = 3*dotlen # seconds
  letterSpace = 3*dotlen # second
  wordSpace = 5*dotlen
  
  for letter in xmtStr:
    c = morse.CODE[letter] # find the Morse representation of a character
    for elem in c: # element (dot or dash) of character
      if(elem == '-'): # dash
        si.enableOutputs(True)
        time.sleep(dashlen)
        si.enableOutputs(False)
        time.sleep(dotlen) # pause between elements
      elif(elem == '.'): # dot
        si.enableOutputs(True)
        time.sleep(dotlen)
        si.enableOutputs(False)
        time.sleep(dotlen) # pause between elements
      else: # spaces and other stuff
        time.sleep(wordSpace)
    time.sleep(letterSpace) # pause between characters

if __name__ == "__main__":
  si = Si5351.Si5351()
  if(len(sys.argv) < 4):
    print 'usage: python Si5351SendCW freqMHz ditTime "transmit string"'
    exit()
  
  xmtStr = sys.argv[3].upper()
  print xmtStr
  
  si.setFreq(si.PLL_A, 0, float(sys.argv[1]))
  sendTxt(xmtStr, float(sys.argv[2]))

